package csv

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/support"
	"testing"
)

func TestPrintCaseDetails(t *testing.T) {
	/**
	Data structure for the Account
	*/
	type AccountItem struct {
		AccountName         string
		AccountType         string
		Description         string
		AccountNumber       int
		AccountSteward      string
		AccountStewardEmail string
		OtherContact        string
		OtherContactEmail   string
		HubRequest          string
		Comment             string
		Portfolio           string
		PortfolioOwner      string
		PortfolioOwnerEmail string
		Team                string
		TeamOwner           string
		TeamOwnerEmail      string
		OrgId				string

	}

	type args struct {
		structvalue interface{}
	}
	tests := []struct {
		name            string
		args            args
		wantReturnValue string
	}{
		// TODO: Add test cases.
		{
			name: "Test with AWS struct (elements of this struct are all pointers)",
			args: args{structvalue: support.CaseDetails{
				CaseId:               aws.String("foo"),
				CategoryCode:         aws.String("foo"),
				CcEmailAddresses:     []*string{aws.String("foo")},
				DisplayId:            aws.String("foo"),
				Language:             aws.String("foo"),
				RecentCommunications: nil,
				ServiceCode:          aws.String("foo"),
				SeverityCode:         aws.String("foo"),
				Status:               aws.String("foo"),
				Subject:              aws.String("foo"),
				SubmittedBy:          aws.String("foo"),
				TimeCreated:          aws.String("foo"),
			}},
			wantReturnValue: `"foo","foo","[]","foo","foo","foo","foo","foo","foo","foo","foo","foo"`,
		},
		{
			name: "Test with my struct, elements of this are all strings",
			args: args{structvalue: AccountItem{
				AccountName:         "foo",
				AccountType:         "foo",
				Description:         "foo",
				AccountNumber:       0,
				AccountSteward:      "foo",
				AccountStewardEmail: "foo",
				OtherContact:        "foo",
				OtherContactEmail:   "foo",
				HubRequest:          "foo",
				Comment:             "foo",
				Portfolio:           "foo",
				PortfolioOwner:      "foo",
				PortfolioOwnerEmail: "foo",
				Team:                "foo",
				TeamOwner:           "foo",
				TeamOwnerEmail:      "foo",
				OrgId:               "foo",
			}},
			wantReturnValue: `"foo","foo","foo","0","foo","foo","foo","foo","foo","foo","foo","foo","foo","foo","foo","foo","foo"`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotReturnValue := GetStructDetailsAsCSV(tt.args.structvalue); gotReturnValue != tt.wantReturnValue {
				t.Errorf("GetStructDetailsAsCSV() = %v, want %v", gotReturnValue, tt.wantReturnValue)
			}
		})
	}
}
