package csv

import (
	"fmt"
	"reflect"
	"strconv"
)

/**
Print the slice of strings as a quoted CSV
*/
func PrintStringSliceAsQuotedCSV(values []string) {
	for i, value := range values {
		// Add the comma for anything after the first one
		if i > 0 {
			fmt.Printf(",")
		}
		// Print the value
		fmt.Printf("%v", strconv.Quote(value))
	}
	fmt.Print("\n")
}

/**
Get the names of the fields in the structure ...
*/
func GetStructNames(structvalue interface{}) (names []string) {
	typeOf := reflect.TypeOf(structvalue)

	// Loop through the fields
	for i := 0; i < typeOf.NumField(); i++ {
		// Bypass the private placeholder
		if typeOf.Field(i).Name == "_"{
			continue
		}
		names = append(names, typeOf.Field(i).Name)
	}
	return
}

func GetStructDetailsAsCSV(structvalue interface{}) (returnValue string) {
	names := GetStructNames(structvalue)
	for i, name := range names {
		//log.Printf("name: %v\n", name)
		if i > 0 {
			returnValue = returnValue + fmt.Sprintf(",")
		}

		s := reflect.ValueOf(structvalue).FieldByName(name).Interface()

		returnValue = returnValue + fmt.Sprintf("\"%v\"",s)
	}
	return

}
