package csv

import (
	"testing"
)

func TestPrintStringSliceAsQuotedCSV(t *testing.T) {
	type args struct {
		values []string
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
		{
			name: "Simple slice",
			args: args{
				values: []string{
					"first",
					"second",
				},
			},
		},
		{
			name: "With quotes slice",
			args: args{
				values: []string{
					`This is a "quoted" line with other quotes in it`,
					"This second line has escaped \"double quotes\"",
				},
			},
		},

	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Probably need to figure out how to check output from
			// this func as it only does a print
			PrintStringSliceAsQuotedCSV(tt.args.values)
		})
	}
}
