package csv

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/support"
	"reflect"
	"testing"
)

func TestGetStructNames(t *testing.T) {
	type args struct {
		Structvalue support.CaseDetails
	}
	tests := []struct {
		name      string
		args      args
		wantNames []string
	}{
		// TODO: Add test cases.
		{
			name: "Test with args struct",
			args: args{Structvalue: support.CaseDetails{
				CaseId:               aws.String("foo"),
				CategoryCode:         nil,
				CcEmailAddresses:     nil,
				DisplayId:            nil,
				Language:             nil,
				RecentCommunications: nil,
				ServiceCode:          nil,
				SeverityCode:         nil,
				Status:               nil,
				Subject:              nil,
				SubmittedBy:          nil,
				TimeCreated:          nil,
			}},
			wantNames: []string{"CaseId", "CategoryCode" ,"CcEmailAddresses", "DisplayId", "Language", "RecentCommunications", "ServiceCode" ,"SeverityCode", "Status" ,"Subject", "SubmittedBy", "TimeCreated"},
		},

	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotNames := GetStructNames(tt.args.Structvalue)
			if !reflect.DeepEqual(gotNames, tt.wantNames) {
				t.Errorf("GetStructNames() = %v, want %v", gotNames, tt.wantNames)
			}
		})
	}
}
